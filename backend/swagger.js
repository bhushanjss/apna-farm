const swaggerAutogen = require('swagger-autogen')();

const doc = {
    info: {
        version: "1.0.0",
        title: "Apna Farm Backend",
        description: "Apna Farm Backend"
    },
    host: "localhost:8080",
    basePath: "/api",
    schemes: ['http', 'https'],
    consumes: ['application/json'],
    produces: ['application/json'],
    tags: [],
    securityDefinitions: {
        
    },
    definitions: {},
    components: {}
};

const outputFile = 'src/swagger.json';
const endpointsFiles = [
    'src/api/agent/index',
    'src/api/answer/index',
    'src/api/auth/index',
    'src/api/farmer/index',
    'src/api/land/index',
    'src/api/location/index',
    'src/api/person/index',
    'src/api/user/index',
];

/* NOTE: if you use the express Router, you must pass in the
   'endpointsFiles' only the root file where the route starts,
   such as index.js, app.js, routes.js, ... */

swaggerAutogen(outputFile, endpointsFiles, doc);