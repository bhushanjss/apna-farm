import React from 'react';
import Spinner from 'src/view/shared/Spinner';
import {getLanguage, i18n} from 'src/i18n';
import FarmerViewItem from 'src/view/farmer/view/FarmerViewItem';
import {Box} from "@material-ui/core";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCellCustom from "../../shared/table/TableCellCustom";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import form from '../../../modules/shared/farmer-form.json';

function AnswerView(props) {
  const renderView = () => {
    const { record } = props;
    const answer = record.answer;
    const answers = JSON.parse(answer);
    const currentLanguageId = getLanguage().id;
    const getLabel = (id) => {
        let answerItem;
        answerItem = form.fields.find( item => (item.id === id));
        if(answerItem) {
            if(currentLanguageId === 'hi') {
                return answerItem.labelHi;
            } else {
                return answerItem.label;
            }
        }
        return id;
    }

    return (
      <div>
        <FarmerViewItem
          label={i18n('entities.answer.fields.farmer')}
          value={record.farmer}
        />
        <Box
          style={{
              display: 'block',
              width: '100%',
              overflowX: 'auto',
          }}
        >
              <Table
                  style={{
                      borderRadius: '5px',
                      border: '1px solid rgb(224, 224, 224)',
                      borderCollapse: 'initial',
                  }}
              >
                  <TableHead>
                      <TableRow>
                          <TableCellCustom
                              name={'question'}
                              label={i18n(
                                  'entities.answer.fields.question',
                              )}
                          />
                          <TableCellCustom
                              name={'answer'}
                              label={i18n(
                                  'entities.answer.fields.answer',
                              )}
                          />
                      </TableRow>
                  </TableHead>
                  <TableBody>
                      {
                          Object.keys(answers).map((id) => {
                              const val = answers[id];
                              const label = getLabel(id) || '';

                              return <TableRow key={id}>
                                  <TableCell>{label}</TableCell>
                                  <TableCell>{val}</TableCell>
                              </TableRow>
                          })
                      }
                  </TableBody>
              </Table>
          </Box>
      </div>
    );
  };

  const { record, loading } = props;

  if (loading || !record) {
    return <Spinner />;
  }

  return renderView();
}

export default AnswerView;
