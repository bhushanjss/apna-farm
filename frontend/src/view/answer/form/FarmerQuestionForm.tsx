import React, { useState } from 'react';
import {Button, Grid, FormLabel, FormGroup, makeStyles} from '@material-ui/core';
import {i18n, getLanguage, getLanguages} from 'src/i18n';

import InputFormItem from 'src/view/shared/form/items/InputFormItem';
import SwitchFormItem from 'src/view/shared/form/items/SwitchFormItem';
import RadioFormItem from 'src/view/shared/form/items/RadioFormItem';

import form from '../../../modules/shared/farmer-form.json';
import personEnumerators from "../../../modules/person/personEnumerators";
import CheckboxFormItem from "../../shared/form/items/CheckboxFormItem";
import moment from "moment";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { Typography } from '@material-ui/core';
import { FormButtons } from 'src/view/shared/styles/FormWrapper';

const useStyles = makeStyles((theme) => ({
    hidden: {
        display: "none"
    },
    show: {
        display: "inherit"
    }
}));


const Questions = (props, state) => {
    const classes = useStyles();
    const maxShow = 5;
    const data = form.fields;
    const [currentPageIdx, setCurrentPageIdx] = useState(0);
    const [disablePrev, setDisablePrev] = useState(false);
    const [disableNext, setDisableNext] = useState(true);
    const totalQLimit = Math.floor(data.length/maxShow);
    const currentLanguageId = getLanguage().id;

    const nextIdx = () => {
        const newPageIdx = currentPageIdx + 1;
        setDisablePrev(true);
        if(newPageIdx <=totalQLimit){
            setCurrentPageIdx(newPageIdx);
            if(newPageIdx===totalQLimit)
                setDisableNext(false);
        }
    }

    const prevIdx = () => {
        const newPageIdx = currentPageIdx - 1;
        setDisableNext(true);
        if(newPageIdx >=0) {
            setCurrentPageIdx(newPageIdx);
            if(newPageIdx===0)
                setDisablePrev(false)
        }
    }

    function getClassVisible(id) {
        const start = currentPageIdx*maxShow;
        let end = (currentPageIdx+1)*maxShow;
        if(end > data.length)
            end = data.length;

        for(let i=start;i<end;i++) {
            const itemId = data[i].id;
            if(itemId===id) {
                return classes.show;
            }
        }
        return classes.hidden;
    }

    return (
        <>
            {data.map((field) => {
                const { id, labelHi, required } = field;
                const options = field.options || [];
                let label = field.label;
                if(currentLanguageId === 'hi') {
                    label = labelHi;
                }

                let questionInput;
                switch (field.type) {
                    case 'CHECKBOX':
                        questionInput = (<FormGroup>
                            <FormLabel component="legend">{label}</FormLabel>
                            {options.map(item => {
                                return <CheckboxFormItem
                                    name={item.label}
                                    label={item.label}
                                />
                            })}
                        </FormGroup>)
                        break
                    case 'RADIO':
                        questionInput = <RadioFormItem
                            name={id}
                            label={label}
                            options={options.map(
                                (value) => ({
                                    value: value.label,
                                    label: value.label
                                }),
                            )}
                            required={required}
                        />
                        break
                    case 'SHORT_ANSWER':
                        questionInput =  <InputFormItem
                            name={id}
                            label={label}
                            required={required}
                        />
                        break
                    // case 'LONG_ANSWER':
                    //     questionInput = <LongAnswerInput id={id} />
                    //     break
                    // case 'RADIO_GRID':
                    //     questionInput = <RadioGridInput id={id} />
                    //     break
                    // case 'CHECKBOX_GRID':
                    //     questionInput = <CheckboxGridInput id={id} />
                    //     break
                    // case 'DROPDOWN':
                    //     questionInput = <DropdownInput id={id} />
                    //     break
                    // case 'LINEAR':
                    //     questionInput = <LinearGrid id={id} />
                    //     break
                    default:
                        questionInput =  <InputFormItem
                            name={id}
                            label={label}
                            required={required}
                        />
                        break
                }

                if (!questionInput) {
                    return null
                }

                return (
                    <Grid className={getClassVisible(id)} item lg={7} md={8} sm={12} xs={12}>
                        {questionInput}
                    </Grid>
                )
            })}
            <Grid style={{ padding: 8 }} xs={12}>
                <Button
                    onClick={prevIdx}
                    type="button"
                    startIcon={<ArrowBackIosIcon />}
                    disabled={!disablePrev}
                    size="small"
                >
                    {i18n('common.prev')}
                </Button>
                <Button
                    onClick={nextIdx}
                    type="button"
                    endIcon={<ArrowForwardIosIcon />}
                    disabled={!disableNext}
                    size="small"
                >
                    {i18n('common.next')}
                </Button>
            </Grid>
        </>
    )
}

function FarmerQuestionForm(props) {

    return <Questions />
}

export default FarmerQuestionForm;