import { Button, TextField, Grid } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import SaveIcon from '@material-ui/icons/Save';
import UndoIcon from '@material-ui/icons/Undo';
import { useForm, FormProvider } from 'react-hook-form';
import { i18n } from 'src/i18n';
import actions from 'src/modules/auth/authActions';
import selectors from 'src/modules/auth/authSelectors';
import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import ImagesFormItem from 'src/view/shared/form/items/ImagesFormItem';
import InputFormItem from 'src/view/shared/form/items/InputFormItem';
import RadioFormItem from 'src/view/shared/form/items/RadioFormItem';
import personEnumerators from 'src/modules/person/personEnumerators';
import DatePickerFormItem from 'src/view/shared/form/items/DatePickerFormItem';
import LocationAutocompleteFormItem from 'src/view/location/autocomplete/LocationAutocompleteFormItem';
import FormWrapper, {
  FormButtons,
} from 'src/view/shared/styles/FormWrapper';
import * as yup from 'yup';
import yupFormSchemas from 'src/modules/shared/yup/yupFormSchemas';
import Storage from 'src/security/storage';
import { yupResolver } from '@hookform/resolvers';
import moment from 'moment';

const phoneRegExp = /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/

const schema = yup.object().shape({
  firstName: yupFormSchemas.string(
    i18n('user.fields.firstName'),
    {
      "min": 2,
      "max": 255,
      "required": true
    },
  ),
  lastName: yupFormSchemas.string(
    i18n('user.fields.lastName'),
    {
      "min": 2,
      "max": 255
    },
  ),
  firstNameFather: yupFormSchemas.string(
    i18n('entities.person.fields.firstNameFather'),
    {
      "min": 2,
      "max": 255,
      "required": true
    },
  ),
  lastNameFather: yupFormSchemas.string(
    i18n('entities.person.fields.lastNameFather'),
    {
      "min": 2,
      "max": 255
    },
  ),
  gender: yupFormSchemas.enumerator(
    i18n('entities.person.fields.gender'),
    {
      "required": true,
      "options": personEnumerators.gender
    },
  ),
  birthdate: yupFormSchemas.date(
    i18n('entities.person.fields.birthdate'),
    {},
  ),
  phoneNumber: yupFormSchemas.string(
    i18n('entities.person.fields.phoneNumber'),
    {
      "required": false,
      "min": 10
    },
  ).matches(phoneRegExp, 'Phone number is not valid'),
  location: yupFormSchemas.relationToOne(
    i18n('entities.person.fields.location'),
    {
      "required": true
    },
  ),
  email: yupFormSchemas.string(
    i18n('entities.person.fields.email'),
    {
      "max": 255
    },
  ).email('Must be a valid email'),
  active: yupFormSchemas.boolean(
    i18n('entities.person.fields.active'),
    {},
  ),
  avatars: yupFormSchemas.images(
    i18n('user.fields.avatars'),
    {
      max: 1,
    },
  ),
});

function ProfileFormPage(props) {
  const dispatch = useDispatch();

  const saveLoading = useSelector(
    selectors.selectLoadingUpdateProfile,
  );

  const currentUser = useSelector(
    selectors.selectCurrentUser,
  );

  const [initialValues] = useState(() => {
    const record = currentUser || {};

    return {
      personId: record.personId,
      firstName: record.firstName,
      lastName: record.lastName,
      firstNameFather: record.firstNameFather,
      lastNameFather: record.lastNameFather,
      gender: record.gender,
      birthdate: record.birthdate ? record.birthdate : '01-01-1990',
      phoneNumber: record.phoneNumber || '+91',
      location: record.location,
      email: record.email,
      active: record.active || true,
      label: `${record.firstName},${record.lastName},${record.personId}`,
      avatars: record.avatars || [],
    };
  });

  const form = useForm({
    resolver: yupResolver(schema),
    mode: 'all',
    defaultValues: initialValues,
  });

  const onSubmit = (values) => {
    const { email, role } = currentUser;

    const {firstName, lastName, firstNameFather, lastNameFather, location } = values;
    const locationAr = location.split(";");
    const recordValues = {...values, role, email, location: locationAr[0], label: `${firstName} ${lastName} S/O  ${firstNameFather} ${lastNameFather}` };
    dispatch(actions.doUpdateProfile(recordValues));
  };

  const onReset = () => {
    Object.keys(initialValues).forEach((key) => {
      form.setValue(key, initialValues[key]);
    });
  };

  return (
    <FormWrapper>
      <FormProvider {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)}>
          <Grid spacing={2} container>
            <Grid item lg={6} md={8} sm={12} xs={12}>
              <InputFormItem
                name="firstName"
                label={i18n('user.fields.firstName')}
                autoComplete="firstName"
                required={true}
                autoFocus
              />
            </Grid>
            <Grid item lg={6} md={8} sm={12} xs={12}>
              <InputFormItem
                name="lastName"
                label={i18n('user.fields.lastName')}
                autoComplete="lastName"
                required={true}
              />
            </Grid>
            <Grid item lg={6} md={8} sm={12} xs={12}>
              <InputFormItem
                name="firstNameFather"
                label={i18n('entities.person.fields.firstNameFather')}  
                required={true}
              />
            </Grid>
            <Grid item lg={6} md={8} sm={12} xs={12}>
              <InputFormItem
                name="lastNameFather"
                label={i18n('entities.person.fields.lastNameFather')}
                required={true}
              />
            </Grid>
            <Grid item lg={6} md={8} sm={12} xs={12}>
              <DatePickerFormItem
                name="birthdate"
                label={i18n('entities.person.fields.birthdate')}
                required={false}
              />
            </Grid>
            <Grid item lg={6} md={8} sm={12} xs={12}>
              <RadioFormItem
                name="gender"
                label={i18n('entities.person.fields.gender')}
                options={personEnumerators.gender.map(
                  (value) => ({
                    value,
                    label: i18n(
                      `entities.person.enumerators.gender.${value}`,
                    ),
                  }),
                )}
                required={true}
              />
            </Grid>            
            <Grid item lg={6} md={8} sm={12} xs={12}>
              <TextField
                id="email"
                name="email"
                label={i18n('user.fields.email')}
                value={currentUser.email}
                fullWidth
                margin="normal"
                InputProps={{
                  readOnly: true,
                }}
                InputLabelProps={{
                  shrink: true,
                }}
                variant="outlined"
                size="small"
              />
            </Grid>
            <Grid item lg={6} md={8} sm={12} xs={12}>
              <InputFormItem
                name="phoneNumber"
                label={i18n('entities.person.fields.phoneNumber')} 
                placeholder="+91 " 
                required={true}
              />
            </Grid>
            <Grid item lg={7} md={8} sm={12} xs={12}>
              <LocationAutocompleteFormItem  
                name="location"
                label={i18n('entities.person.fields.location')}
                required={true}
                showCreate={!props.modal}
              />
            </Grid>
            <Grid item lg={6} md={8} sm={12} xs={12}>
              <ImagesFormItem
                name="avatars"
                label={i18n('user.fields.avatars')}
                storage={Storage.values.userAvatarsProfiles}
                max={1}
              />
            </Grid>
          </Grid>

          <FormButtons>
            <Button
              variant="contained"
              color="primary"
              disabled={saveLoading}
              type="button"
              onClick={form.handleSubmit(onSubmit)}
              startIcon={<SaveIcon />}
              size="small"
            >
              {i18n('common.save')}
            </Button>

            <Button
              disabled={saveLoading}
              onClick={onReset}
              type="button"
              startIcon={<UndoIcon />}
              size="small"
            >
              {i18n('common.reset')}
            </Button>

            {props.onCancel ? (
              <Button
                disabled={saveLoading}
                onClick={() => props.onCancel()}
                type="button"
                startIcon={<CloseIcon />}
                size="small"
              >
                {i18n('common.cancel')}
              </Button>
            ) : null}
          </FormButtons>
        </form>
      </FormProvider>
    </FormWrapper>
  );
}

export default ProfileFormPage;
