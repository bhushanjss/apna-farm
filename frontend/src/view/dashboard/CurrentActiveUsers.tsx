import React from 'react';
import { Bar } from 'react-chartjs-2';
import { i18n } from 'src/i18n';

const data = {
  labels: [
    `${i18n('dashboard.charts.agent')}`,
    `${i18n('dashboard.charts.farmer')}`,
    `${i18n('dashboard.charts.researcher')}`,
    `${i18n('dashboard.charts.admin')}`
  ],
  datasets: [
    {
      label: i18n('dashboard.charts.blue'),
      backgroundColor: 'rgba(33,150,243,1)',
      borderColor: 'rgba(255,99,132,1)',
      borderWidth: 1,
      hoverBackgroundColor: 'rgba(255,99,132,0.4)',
      hoverBorderColor: 'rgba(255,99,132,1)',
      data: [5, 20, 5, 3],
    },
  ],
};

const options = {
  legend: {
    display: true,
  },
  scales: {
    xAxes: [
      {
        display: false,
      },
    ],
    yAxes: [
      {
        display: true,
      },
    ],
  },
};

export default function DashboardBarChart(props) {
  return (
    <Bar
      data={data}
      options={options}
      width={100}
      height={50}
    />
  );
}
