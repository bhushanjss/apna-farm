import stateDist from '../shared/state-districts.json';

const locationEnumerators = {
    state: stateDist
};

export default locationEnumerators;
