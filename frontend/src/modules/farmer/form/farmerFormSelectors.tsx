import { createSelector } from 'reselect';

const selectRaw = (state) => state.farmer.form;
const selectUser = (state) => state.auth.currentUser;


const selectRecord = createSelector(
  [selectRaw, selectUser],
  (raw, user) => ({...raw.record, agent: user.agent}),
);

const selectInitLoading = createSelector(
  [selectRaw],
  (raw) => Boolean(raw.initLoading),
);

const selectSaveLoading = createSelector(
  [selectRaw],
  (raw) => Boolean(raw.saveLoading),
);

export default {
  selectInitLoading,
  selectSaveLoading,
  selectRecord,
  selectRaw,
};
