import service from 'src/modules/auth/authService';
import AgentService from "../agent/agentService";
import PersonService from 'src/modules/person/personService';
import Errors from 'src/modules/shared/error/errors';
import Message from 'src/view/shared/message';
import { i18n } from 'src/i18n';
import { getHistory } from 'src/modules/store';
import { AuthToken } from 'src/modules/auth/authToken';
import AuthCurrentTenant from 'src/modules/auth/authCurrentTenant';
import selectors from 'src/modules/auth/authSelectors';
import { tenantSubdomain } from '../tenant/tenantSubdomain';

const prefix = 'AUTH';

const actions = {
  ERROR_MESSAGE_CLEARED: `${prefix}_ERROR_MESSAGE_CLEARED`,

  AUTH_INIT_SUCCESS: `${prefix}_INIT_SUCCESS`,
  AUTH_INIT_ERROR: `${prefix}_INIT_ERROR`,

  AUTH_START: `${prefix}_START`,
  AUTH_SUCCESS: `${prefix}_SUCCESS`,
  AUTH_ERROR: `${prefix}_ERROR`,

  AUTH_SAVE_TENANT: `${prefix}_SAVE_TENANT`,

  UPDATE_PROFILE_START: `${prefix}_UPDATE_PROFILE_START`,
  UPDATE_PROFILE_SUCCESS: `${prefix}_UPDATE_PROFILE_SUCCESS`,
  UPDATE_PROFILE_ERROR: `${prefix}_UPDATE_PROFILE_ERROR`,

  PASSWORD_CHANGE_START: `${prefix}_PASSWORD_CHANGE_START`,
  PASSWORD_CHANGE_SUCCESS: `${prefix}_PASSWORD_CHANGE_SUCCESS`,
  PASSWORD_CHANGE_ERROR: `${prefix}_PASSWORD_CHANGE_ERROR`,

  CURRENT_USER_REFRESH_START: `${prefix}_CURRENT_USER_REFRESH_START`,
  CURRENT_USER_REFRESH_SUCCESS: `${prefix}_CURRENT_USER_REFRESH_SUCCESS`,
  CURRENT_USER_REFRESH_ERROR: `${prefix}_CURRENT_USER_REFRESH_ERROR`,

  PASSWORD_RESET_EMAIL_START: `${prefix}_PASSWORD_RESET_EMAIL_START`,
  PASSWORD_RESET_EMAIL_SUCCESS: `${prefix}_PASSWORD_RESET_EMAIL_SUCCESS`,
  PASSWORD_RESET_EMAIL_ERROR: `${prefix}_PASSWORD_RESET_EMAIL_ERROR`,

  PASSWORD_RESET_START: `${prefix}_PASSWORD_RESET_START`,
  PASSWORD_RESET_SUCCESS: `${prefix}_PASSWORD_RESET_SUCCESS`,
  PASSWORD_RESET_ERROR: `${prefix}_PASSWORD_RESET_ERROR`,

  EMAIL_VERIFY_START: `${prefix}_EMAIL_VERIFY_START`,
  EMAIL_VERIFY_SUCCESS: `${prefix}_EMAIL_VERIFY_SUCCESS`,
  EMAIL_VERIFY_ERROR: `${prefix}_EMAIL_VERIFY_ERROR`,

  EMAIL_CONFIRMATION_START: `${prefix}_EMAIL_CONFIRMATION_START`,
  EMAIL_CONFIRMATION_SUCCESS: `${prefix}_EMAIL_CONFIRMATION_SUCCESS`,
  EMAIL_CONFIRMATION_ERROR: `${prefix}_EMAIL_CONFIRMATION_ERROR`,

  doClearErrorMessage() {
    return {
      type: actions.ERROR_MESSAGE_CLEARED,
    };
  },

  doSendEmailConfirmation: () => async (
    dispatch,
    getState,
  ) => {
    try {
      dispatch({ type: actions.EMAIL_CONFIRMATION_START });
      await service.sendEmailVerification();
      Message.success(
        i18n('auth.verificationEmailSuccess'),
      );
      dispatch({
        type: actions.EMAIL_CONFIRMATION_SUCCESS,
      });
    } catch (error) {
      Errors.handle(error);
      dispatch({ type: actions.EMAIL_CONFIRMATION_ERROR });
    }
  },

  doSendPasswordResetEmail: (email) => async (dispatch) => {
    try {
      dispatch({
        type: actions.PASSWORD_RESET_EMAIL_START,
      });
      await service.sendPasswordResetEmail(email);
      Message.success(
        i18n('auth.passwordResetEmailSuccess'),
      );
      dispatch({
        type: actions.PASSWORD_RESET_EMAIL_SUCCESS,
      });
    } catch (error) {
      Errors.handle(error);
      dispatch({
        type: actions.PASSWORD_RESET_EMAIL_ERROR,
      });
    }
  },

  doRegisterEmailAndPassword: (email, password) => async (
    dispatch,
  ) => {
    try {
      dispatch({ type: actions.AUTH_START });

      const token = await service.registerWithEmailAndPassword(
        email,
        password,
      );

      AuthToken.set(token, true);
      let currentUser = await service.fetchMe();
      dispatch({
        type: actions.AUTH_SAVE_TENANT,
        payload: {
          currentUser,
        },
      });
      const personId  = currentUser["personId"];
      if(personId) {
        const personRecord = await PersonService.find(personId);
        currentUser = {...personRecord, ...currentUser}
      }
      currentUser = await getAgent(currentUser);

      dispatch({
        type: actions.AUTH_SUCCESS,
        payload: {
          currentUser,
        },
      });
      if(currentUser && !currentUser["personId"])
      {
        getHistory().push('/profile');
      }
    } catch (error) {
      await service.signout();

      if (Errors.errorCode(error) !== 400) {
        Errors.handle(error);
      }

      dispatch({
        type: actions.AUTH_ERROR,
        payload: Errors.selectMessage(error),
      });
    }
  },

  doSigninWithEmailAndPassword: (
    email,
    password,
    rememberMe,
  ) => async (dispatch) => {

    try {
      dispatch({ type: actions.AUTH_START });

      const token = await service.signinWithEmailAndPassword(
        email,
        password,
      );

      AuthToken.set(token, rememberMe);
      let currentUser = await service.fetchMe();

      dispatch({
        type: actions.AUTH_SAVE_TENANT,
        payload: {
          currentUser,
        },
      });

      const personId  = currentUser["personId"];
      if(personId) {
        const personRecord = await PersonService.find(personId);
        currentUser = {...personRecord, ...currentUser}
      }

      currentUser = await getAgent(currentUser);

      dispatch({
        type: actions.AUTH_SUCCESS,
        payload: {
          currentUser,
        },
      });

      if(currentUser && !currentUser["personId"])
      {
        getHistory().push('/profile');
      }
    } catch (error) {
      await service.signout();

      if (Errors.errorCode(error) !== 400) {
        Errors.handle(error);
      }

      dispatch({
        type: actions.AUTH_ERROR,
        payload: Errors.selectMessage(error),
      });
    }
  },

  doSignout: () => async (dispatch) => {
    try {
      dispatch({ type: actions.AUTH_START });
      await service.signout();

      dispatch({
        type: actions.AUTH_SUCCESS,
        payload: {
          currentUser: null,
        },
      });
    } catch (error) {
      Errors.handle(error);

      dispatch({
        type: actions.AUTH_ERROR,
      });
    }
  },

  doInit: () => async (dispatch) => {
    try {
      const token = AuthToken.get();

      if (token) {
        let currentUser = await service.fetchMe();
        dispatch({
          type: actions.AUTH_SAVE_TENANT,
          payload: {
            currentUser,
          },
        });
        const personId = currentUser["personId"];
        if(personId) {
          const personRecord = await PersonService.find(personId);
          currentUser = {...personRecord, ...currentUser}
        }
        currentUser = await getAgent(currentUser);
        dispatch({
          type: actions.AUTH_INIT_SUCCESS,
          payload: {
            currentUser,
          },
        });
      } else {
        dispatch({
          type: actions.AUTH_INIT_SUCCESS,
          payload: {
            currentUser: null,
          },
        });
      }

    } catch (error) {
      service.signout();
      Errors.handle(error);

      dispatch({
        type: actions.AUTH_INIT_ERROR,
        payload: error,
      });
    }
  },

  doRefreshCurrentUser: () => async (dispatch) => {
    try {
      dispatch({
        type: actions.CURRENT_USER_REFRESH_START,
      });

      const token = AuthToken.get();

      if (token) {
        let currentUser = await service.fetchMe();
        dispatch({
          type: actions.AUTH_SAVE_TENANT,
          payload: {
            currentUser,
          },
        });
        const personId  = currentUser["personId"];
        if(personId) {
          const personRecord = await PersonService.find(personId);
          currentUser = {...personRecord, ...currentUser}
        }
        currentUser = await getAgent(currentUser);

        dispatch({
          type: actions.CURRENT_USER_REFRESH_SUCCESS,
          payload: {
            currentUser,
          },
        });
      } else {
        dispatch({
          type: actions.CURRENT_USER_REFRESH_SUCCESS,
          payload: {
            currentUser: null,
          },
        });
      }

    } catch (error) {
      service.signout();
      Errors.handle(error);

      dispatch({
        type: actions.CURRENT_USER_REFRESH_ERROR,
        payload: error,
      });
    }
  },

  doUpdateProfile: (data) => async (dispatch) => {
    try {
      dispatch({
        type: actions.UPDATE_PROFILE_START,
      });
      
      const { personId, firstName, lastName, firstNameFather, lastNameFather, gender, birthdate, 
        phoneNumber, location, email, label, avatars, role } = data;

      let personPayload = { firstName, lastName, firstNameFather, lastNameFather, gender, birthdate, 
        phoneNumber, location, email, active: true, label };

      let personResponse;
      if(personId) {
        personPayload['personId'] = personId;
        personResponse = await PersonService.update(personId, personPayload);        
      } else {
        personResponse = await PersonService.create(personPayload);
        if(role === 'agent') {
          const agentPayload = {
            farmers: [],
            label: label,
            locations: [location],
            person: personResponse.id
          };
          await AgentService.create(agentPayload);
        }
      }

      const profilePayload = {firstName, lastName, phoneNumber, email, avatars, personId: personResponse.id};
      await service.updateProfile(profilePayload);



      dispatch({
        type: actions.UPDATE_PROFILE_SUCCESS,
      });
      await dispatch(actions.doRefreshCurrentUser());
      Message.success(i18n('auth.profile.success'));
      getHistory().push('/');
    } catch (error) {
      Errors.handle(error);

      dispatch({
        type: actions.UPDATE_PROFILE_ERROR,
      });
    }
  },

  doChangePassword: (oldPassword, newPassword) => async (
    dispatch,
  ) => {
    try {
      dispatch({
        type: actions.PASSWORD_CHANGE_START,
      });

      await service.changePassword(
        oldPassword,
        newPassword,
      );

      dispatch({
        type: actions.PASSWORD_CHANGE_SUCCESS,
      });
      await dispatch(actions.doRefreshCurrentUser());
      Message.success(i18n('auth.passwordChange.success'));
      getHistory().push('/');
    } catch (error) {
      Errors.handle(error);

      dispatch({
        type: actions.PASSWORD_CHANGE_ERROR,
      });
    }
  },

  doVerifyEmail: (token) => async (dispatch, getState) => {
    try {
      const isLoading = selectors.selectLoadingVerifyEmail(
        getState(),
      );

      if (isLoading) {
        return;
      }

      dispatch({
        type: actions.EMAIL_VERIFY_START,
      });

      await service.verifyEmail(token);

      await dispatch(actions.doRefreshCurrentUser());

      dispatch({
        type: actions.EMAIL_VERIFY_SUCCESS,
      });

      getHistory().push('/');
    } catch (error) {
      Errors.showMessage(error);
      dispatch({
        type: actions.EMAIL_VERIFY_ERROR,
      });
      getHistory().push('/');
    }
  },

  doResetPassword: (token, password) => async (
    dispatch,
  ) => {
    try {
      dispatch({
        type: actions.PASSWORD_RESET_START,
      });

      await service.passwordReset(token, password);

      Message.success(i18n('auth.passwordResetSuccess'));
      dispatch({
        type: actions.PASSWORD_RESET_SUCCESS,
      });
      getHistory().push('/');
    } catch (error) {
      Errors.handle(error);

      dispatch({
        type: actions.PASSWORD_RESET_ERROR,
      });

      dispatch(actions.doSignout());
      getHistory().push('/');
    }
  },

  doSelectTenant: (tenant) => async (dispatch) => {
    if (tenantSubdomain.isEnabled) {
      tenantSubdomain.redirectAuthenticatedTo(tenant.url);
      return;
    }

    AuthCurrentTenant.set(tenant);
    await dispatch(actions.doRefreshCurrentUser());
    getHistory().push('/');
  },
};

const getRole = (currentUser) => currentUser.tenants[0].roles[0];

const getAgent = async (currentUser) => {
  const role = getRole(currentUser);
  let user = {...currentUser, role};

  if(role==='agent') {
    const personId = currentUser.personId;
    if(personId) {
      const agentObj = await AgentService.findByPerson(personId);
      const agent = agentObj.id;
      if(agent) {
        user = {...user, agent };
      }
    }
  }
  return user;
}

export default actions;
