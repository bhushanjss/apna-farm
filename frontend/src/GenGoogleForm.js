const reactGoogleFormsHooks = require("react-google-forms-hooks")
const fs = require('fs');

// can use both full and shortened form url
reactGoogleFormsHooks.googleFormsToJson('https://docs.google.com/forms/d/e/1FAIpQLScyyeefyyff4cLzNlqh3j9zIYIzDrkYcB-cPA8noWunZJ0ARw/viewform'
).then(data => {
    // console.log(data.fields)
    let formData = JSON.stringify(data);
    fs.writeFileSync('./src/modules/shared/farmer-form.json', formData);
    console.log("Form Data Saved...")
})

